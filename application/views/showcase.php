<?php

defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head> 
<title>Showcase</title>
<style type="text/css">
  img{
    height: 100px;  
  }
</style>
</head>
<body>
<?php
      
        $query = $this->db->query("SELECT title, description, link, img_url FROM showcase ORDER BY post_id DESC");
        foreach ($query->result() as $row)
      {
        echo "<h1>".$row->title."</h1>";
        echo '<img src="'.base_url($row->img_url).'">';
        echo "<p>".$row->description."</p>";
        echo '<a href="'.$row->link.'">Read</a>';
        echo '<br>';
        
      }

  
?>

</body>
</html>