<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head> 
 <!-- Site made with Mobirise Website Builder v3.12.1, https://mobirise.com -->
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="generator" content="Mobirise v3.12.1, mobirise.com">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" href="<?php echo base_url('assets/images/logo2-505x128-5.png');?>" type="image/x-icon">
  <meta name="description" content="">
  
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic&amp;subset=latin">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,700">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i">
  <link rel="stylesheet" href="<?php echo base_url('assets/mobirise/bootstrap-material-design-font/css/material.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/mobirise/et-line-font-plugin/style.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/mobirise/tether/tether.min.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/mobirise/bootstrap/css/bootstrap.min.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/mobirise/dropdown/css/style.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/mobirise/animate.css/animate.min.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/mobirise/socicon/css/styles.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/mobirise/theme/css/style.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/mobirise/mobirise3-blocks-plugin/css/style.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/mobirise/mobirise-gallery/style.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/mobirise/css/mbr-additional.css');?>" type="text/css">
  
</head>
<body>
<section id="ext_menu-0">

    <nav class="navbar navbar-dropdown bg-color transparent navbar-fixed-top">
        <div class="container">

            <div class="mbr-table">
                <div class="mbr-table-cell">

                    <div class="navbar-brand">
                        <a href="https://akaar.org" class="navbar-logo"><img src="<?php echo base_url('assets/images/logo2-505x128-5.png');?>" alt="Mobirise"></a>
                        
                    </div>

                </div>
                <div class="mbr-table-cell">

                    <button class="navbar-toggler pull-xs-right hidden-md-up" type="button" data-toggle="collapse" data-target="#exCollapsingNavbar">
                        <div class="hamburger-icon"></div>
                    </button>

                    <ul class="nav-dropdown collapse pull-xs-right nav navbar-nav navbar-toggleable-sm" id="exCollapsingNavbar"><li class="nav-item"><a class="nav-link link" href="https://mobirise.com/"><br></a></li><li class="nav-item dropdown"><a class="nav-link link dropdown-toggle" href="https://mobirise.com/" data-toggle="dropdown-submenu" aria-expanded="false">SERVICES</a><div class="dropdown-menu"><a class="dropdown-item" href="http://www.akaar.org/laser" target="_blank">LASER CUTTING</a><a class="dropdown-item" href="http://www.akaar.org/printing_3d" target="_blank">3D PRINTING</a><a class="dropdown-item" href="http://www.akaar.org/cnc_routing" target="_blank">CNC ROUTING</a><a class="dropdown-item" href="http://www.akaar.org/cnc_machining" target="_blank">CNC MACHINING</a><a class="dropdown-item" href="http://www.akaar.org/nameboards" target="_blank">NAMEBOARDS</a><a class="dropdown-item" href="http://www.akaar.org/metal-fabrication" target="_blank">METAL FABRICATION</a><a class="dropdown-item" href="http://www.akaar.org/acrylic-fabrication" target="_blank">ACRYLIC FABRICATION</a><a class="dropdown-item" href="http://www.akaar.org/custom-manufacturing" target="_blank">CUSTOMISED PRODUCT</a></div></li><li class="nav-item"><a class="nav-link link" href="http://www.akaar.org/blog" target="_blank">BLOG</a></li><li class="nav-item"><a class="nav-link link" href="http://www.akaar.org/user/show_user_registration" target="_blank">SIGN UP</a></li><li class="nav-item"><a class="nav-link link" href="tel:180030025227" target="_blank"><span class="socicon socicon-viber mbr-iconfont mbr-iconfont-btn"></span> 1800 300 25227</a></li><li class="nav-item nav-btn"><a class="nav-link btn btn-primary" href="index.html#form1-e">GET A FREE ESTIMATE NOW</a></li></ul>
                    <button hidden="" class="navbar-toggler navbar-close" type="button" data-toggle="collapse" data-target="#exCollapsingNavbar">
                        <div class="close-icon"></div>
                    </button>

                </div>
            </div>

        </div>
    </nav>

</section>

<section class="engine"><a rel="external" href="https://mobirise.com">Site Builder</a></section><section class="mbr-section mbr-section-hero mbr-section-full mbr-parallax-background mbr-section-with-arrow mbr-after-navbar" id="header4-1" style="background-image: url(assets/images/giphy-downsized-large.gif);">

    <div class="mbr-overlay" style="opacity: 0.7; background-color: rgb(0, 0, 0);"></div>

    <div class="mbr-table-cell">

        <div class="container">
            <div class="row">
                <div class="mbr-section col-md-10 col-md-offset-2 text-xs-right">

                    <h1 class="mbr-section-title display-1">Customised Products</h1>
                    <p class="mbr-section-lead lead">Have a revolutionary product idea?  Or looking for prototyping a new product. With Akaar you can do it all. We offer a large range of manufacturing services and design solutions to make your vision come to life.</p>
                    <div class="mbr-section-btn"><a class="btn btn-lg btn-success" href="index.html#form1-e">SUBMIT DESIGN</a> </div>
                </div>
            </div>
        </div>
    </div>

    <div class="mbr-arrow mbr-arrow-floating" aria-hidden="true"><a href="#header3-7"><i class="mbr-arrow-icon"></i></a></div>

</section>

<section class="mbr-section mbr-section__container article" id="header3-7" style="background-color: rgb(255, 255, 255); padding-top: 40px; padding-bottom: 20px;">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h3 class="mbr-section-title display-2">Services with Akaar</h3>
                
            </div>
        </div>
    </div>
</section>

<section class="mbr-cards mbr-section mbr-section-nopadding" id="features3-6" style="background-color: rgb(255, 255, 255);">

    

    <div class="mbr-cards-row row">
        <div class="mbr-cards-col col-xs-12 col-lg-2" style="padding-top: 0px; padding-bottom: 40px;">
            <div class="container">
              <div class="card cart-block">
                  <div class="card-img"><img src="<?php echo base_url('assets/images/selective-laser-sintering-sls-large-600x600.jpg');?>" class="card-img-top"></div>
                  <div class="card-block">
                    <h4 class="card-title">3D Printing</h4>
                    
                    
                    <div class="card-btn"><a href="http://www.akaar.org/printing_3d" class="btn btn-primary" target="_blank">MORE</a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mbr-cards-col col-xs-12 col-lg-2" style="padding-top: 0px; padding-bottom: 40px;">
            <div class="container">
                <div class="card cart-block">
                    <div class="card-img"><img src="<?php echo base_url('assets/images/istock-000006450956-xxxlarge-600x600.jpg');?>" class="card-img-top"></div>
                    <div class="card-block">
                        <h4 class="card-title">WaterJet Cutting</h4>
                        
                        
                        <div class="card-btn"><a href="http://www.akaar.org/laser" class="btn btn-primary" target="_blank">MORE</a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mbr-cards-col col-xs-12 col-lg-2" style="padding-top: 0px; padding-bottom: 40px;">
            <div class="container">
                <div class="card cart-block">
                    <div class="card-img"><img src="<?php echo base_url('assets/images/36496f4fe3ea02aa42fd75c75459c438-laser-cutting-cuttings-600x600.jpg');?>" class="card-img-top"></div>
                    <div class="card-block">
                        <h4 class="card-title">Laser Cutting</h4>
                        
                        
                        <div class="card-btn"><a href="http://www.akaar.org/laser" class="btn btn-primary">MORE</a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mbr-cards-col col-xs-12 col-lg-2" style="padding-top: 0px; padding-bottom: 40px;">
            <div class="container">
                <div class="card cart-block">
                    <div class="card-img"><img src="<?php echo base_url('assets/images/cnc-milling-600x600.jpg');?>" class="card-img-top"></div>
                    <div class="card-block">
                        <h4 class="card-title">CNC Machining</h4>
                        
                        
                        <div class="card-btn"><a href="http://www.akaar.org/cnc_machining" class="btn btn-primary" target="_blank">MORE</a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mbr-cards-col col-xs-12 col-lg-2" style="padding-top: 0px; padding-bottom: 40px;">
            <div class="container">
                <div class="card cart-block">
                    <div class="card-img"><img src="<?php echo base_url('assets/images/20140712045009735-600x600.jpg');?>" class="card-img-top"></div>
                    <div class="card-block">
                        <h4 class="card-title">CNC Routing</h4>
                        
                        
                        <div class="card-btn"><a href="http://www.akaar.org/cnc_routing" class="btn btn-primary" target="_blank">MORE</a></div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</section>

<section class="mbr-section extMsg-box3" id="extMsg-box3-9" style="background-color: rgb(46, 46, 46); padding-top: 50px; padding-bottom: 50px;">

    
    <div class="container">
        <div class="row">
            <div class="mbr-table-md-up">

              <div class="mbr-table-cell mbr-right-padding-md-up mbr-valign-top col-md-6 image-size" style="width: 41%;">
                  <div class="mbr-figure"><img src="<?php echo base_url('assets/images/2138545091-380x383.png');?>"></div>
              </div>

              


              <div class="mbr-table-cell col-md-6 text-xs-center text-md-left content-size">
                  <div class="span-title">
                      <span class="mbr-section-title display-2 text-center pad-r">CUSTOM &nbsp;</span><br>
                      <span class="mbr-section-title display-2 element typedextMsg-box3-9" adress="typedextMsg-box3-9" firstel="3D PRINTED PARTS." secondel="FABRICATED PARTS." thirdel="PROTOTYPING PARTS." typespeed="50"></span><br>
                      <span class="mbr-section-title display-2 text-center pad-r">FIND IT HERE!</span>             
                  </div>             
                  <div class="lead">

                    <p>All you need is a rough idea of your product. Our team at Akaar will help you through selecting the best manufacturing method and wihtin days your product will be delivered to you.&nbsp;</p>

                  </div>

                  <div class="left-button"><a class="btn btn-primary" href="index.html#form1-e">GET A FREE ESTIMATE NOW</a> <a class="btn btn-primary" href="tel:180030025227">SUBMIT DESIGN</a></div>
              </div>


              

            </div>
        </div>
    </div>

</section>

<section class="mbr-section" id="msg-box5-4" style="background-color: rgb(255, 255, 255); padding-top: 80px; padding-bottom: 40px;">

    
    <div class="container">
        <div class="row">
            <div class="mbr-table-md-up">

              

              <div class="mbr-table-cell col-md-5 text-xs-center text-md-right content-size">
                  <h3 class="mbr-section-title display-2">Design Support&nbsp;</h3>
                  <div class="lead">

                    <p>Share with us whats on your mind and &nbsp;we will connect you to a designer.&nbsp;</p>

                  </div>

                  <div><a class="btn btn-success" href="index.html#form1-e">GET A FREE ESTIMATE NOW</a></div>
              </div>


              


              <div class="mbr-table-cell mbr-left-padding-md-up mbr-valign-top col-md-7 image-size" style="width: 52%;">
                  <div class="mbr-figure"><img src="<?php echo base_url('assets/images/handsome-hipster-modern-man-designer-working-home-using-laptop-at-home-1400x931.jpg');?>"></div>
              </div>

            </div>
        </div>
    </div>

</section>

<section class="mbr-section mbr-section__container article" id="header3-i" style="background-color: rgb(255, 255, 255); padding-top: 20px; padding-bottom: 20px;">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h3 class="mbr-section-title display-2">Some of our products&nbsp;</h3>
                
            </div>
        </div>
    </div>
</section>

<section class="mbr-gallery mbr-section mbr-section-nopadding mbr-slider-carousel" id="gallery3-g" data-filter="false" style="background-color: rgb(255, 255, 255); padding-top: 0rem; padding-bottom: 1.5rem;">
    <!-- Filter -->
    

    <!-- Gallery -->
    <div class="mbr-gallery-row">
        <div class=" mbr-gallery-layout-default">
            <div>
                <div>
                    <div class="mbr-gallery-item mbr-gallery-item__mobirise3 mbr-gallery-item--p1" data-tags="Awesome" data-video-url="false">
                        <div href="#lb-gallery3-g" data-slide-to="0" data-toggle="modal">
                            
                            

                            <img alt="" src="<?php echo base_url('assets/images/12762-2000x1334-800x533.jpg');?>">
                            
                            <span class="icon-focus"></span>
                            <span class="mbr-gallery-title">Momento/Trophies</span>
                        </div>
                    </div><div class="mbr-gallery-item mbr-gallery-item__mobirise3 mbr-gallery-item--p1" data-tags="Responsive" data-video-url="false">
                        <div href="#lb-gallery3-g" data-slide-to="1" data-toggle="modal">
                            
                            

                            <img alt="" src="<?php echo base_url('assets/images/048025-2000x1332-800x532.jpg');?>">
                            
                            <span class="icon-focus"></span>
                            <span class="mbr-gallery-title">Signboard</span>
                        </div>
                    </div><div class="mbr-gallery-item mbr-gallery-item__mobirise3 mbr-gallery-item--p1" data-tags="Creative" data-video-url="false">
                        <div href="#lb-gallery3-g" data-slide-to="2" data-toggle="modal">
                            
                            

                            <img alt="" src="<?php echo base_url('assets/images/signage-2000x1332-800x532.jpg');?>">
                            
                            <span class="icon-focus"></span>
                            <span class="mbr-gallery-title">LED Signboard</span>
                        </div>
                    </div><div class="mbr-gallery-item mbr-gallery-item__mobirise3 mbr-gallery-item--p1" data-tags="Animated" data-video-url="false">
                        <div href="#lb-gallery3-g" data-slide-to="3" data-toggle="modal">
                            
                            

                            <img alt="" src="<?php echo base_url('assets/images/transform-acrylic-lucite-coffee-table-with-home-decor-arrangement-ideas-with-acrylic-lucite-coffee-table-2000x1331-800x532.jpg');?>">
                            
                            <span class="icon-focus"></span>
                            <span class="mbr-gallery-title">&nbsp;Acrylic Home decor</span>
                        </div>
                    </div><div class="mbr-gallery-item mbr-gallery-item__mobirise3 mbr-gallery-item--p1" data-tags="Awesome" data-video-url="false">
                        <div href="#lb-gallery3-g" data-slide-to="4" data-toggle="modal">
                            
                            

                            <img alt="" src="<?php echo base_url('assets/images/eidos-dda-kiosks-for-hounslow-ccg-2000x1332-800x532.jpg');?>">
                            
                            <span class="icon-focus"></span>
                            <span class="mbr-gallery-title">Retail kiosks</span>
                        </div>
                    </div><div class="mbr-gallery-item mbr-gallery-item__mobirise3 mbr-gallery-item--p1" data-tags="Beautiful" data-video-url="false">
                        <div href="#lb-gallery3-g" data-slide-to="5" data-toggle="modal">
                            
                            

                            <img alt="" src="<?php echo base_url('assets/images/77523df0bad9ec4b289b3ace0bedcd0c-2000x1333-800x533.jpg');?>">
                            
                            <span class="icon-focus"></span>
                            <span class="mbr-gallery-title">LED Back Acrylic Patterns</span>
                        </div>
                    </div><div class="mbr-gallery-item mbr-gallery-item__mobirise3 mbr-gallery-item--p1" data-tags="Responsive" data-video-url="false">
                        <div href="#lb-gallery3-g" data-slide-to="6" data-toggle="modal">
                            
                            

                            <img alt="" src="<?php echo base_url('assets/images/11196-2000x1331-800x532.jpg');?>">
                            
                            <span class="icon-focus"></span>
                            <span class="mbr-gallery-title">Stands/Poster Stands</span>
                        </div>
                    </div><div class="mbr-gallery-item mbr-gallery-item__mobirise3 mbr-gallery-item--p1" data-tags="Animated" data-video-url="false">
                        <div href="#lb-gallery3-g" data-slide-to="7" data-toggle="modal">
                            
                            

                            <img alt="" src="<?php echo base_url('assets/images/stainless-steel-security-guard-booth-manufacture-hot-2000x1332-800x532.jpg');?>">
                            
                            <span class="icon-focus"></span>
                            <span class="mbr-gallery-title">Kiosk</span>
                        </div>
                    </div><div class="mbr-gallery-item mbr-gallery-item__mobirise3 mbr-gallery-item--p1" data-tags="Animated" data-video-url="false">
                        <div href="#lb-gallery3-g" data-slide-to="8" data-toggle="modal">
                            
                            

                            <img alt="" src="<?php echo base_url('assets/images/13315682-1728962734009842-8039578979391214225-n-2000x1333-800x533.jpg');?>">
                            
                            <span class="icon-focus"></span>
                            <span class="mbr-gallery-title">Metal signboards<br></span>
                        </div>
                    </div><div class="mbr-gallery-item mbr-gallery-item__mobirise3 mbr-gallery-item--p1" data-tags="Animated" data-video-url="false">
                        <div href="#lb-gallery3-g" data-slide-to="9" data-toggle="modal">
                            
                            

                            <img alt="" src="<?php echo base_url('assets/images/5-2000x1334-800x533.jpg');?>">
                            
                            <span class="icon-focus"></span>
                            <span class="mbr-gallery-title">Electric enclosures<br></span>
                        </div>
                    </div><div class="mbr-gallery-item mbr-gallery-item__mobirise3 mbr-gallery-item--p1" data-tags="Animated" data-video-url="false">
                        <div href="#lb-gallery3-g" data-slide-to="10" data-toggle="modal">
                            
                            

                            <img alt="" src="<?php echo base_url('assets/images/d-k-fabs-image-2000x1331-800x532.jpg');?>">
                            
                            <span class="icon-focus"></span>
                            <span class="mbr-gallery-title">Prototyping<br></span>
                        </div>
                    </div><div class="mbr-gallery-item mbr-gallery-item__mobirise3 mbr-gallery-item--p1" data-tags="Animated" data-video-url="false">
                        <div href="#lb-gallery3-g" data-slide-to="11" data-toggle="modal">
                            
                            

                            <img alt="" src="<?php echo base_url('assets/images/20130814-2000x1332-800x532.jpg');?>">
                            
                            <span class="icon-focus"></span>
                            <span class="mbr-gallery-title">3D Printed Parts<br></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

    <!-- Lightbox -->
    <div data-app-prevent-settings="" class="mbr-slider modal fade carousel slide" tabindex="-1" data-keyboard="true" data-interval="false" id="lb-gallery3-g">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <ol class="carousel-indicators">
                        <li data-app-prevent-settings="" data-target="#lb-gallery3-g" data-slide-to="0"></li><li data-app-prevent-settings="" data-target="#lb-gallery3-g" data-slide-to="1"></li><li data-app-prevent-settings="" data-target="#lb-gallery3-g" data-slide-to="2"></li><li data-app-prevent-settings="" data-target="#lb-gallery3-g" data-slide-to="3"></li><li data-app-prevent-settings="" data-target="#lb-gallery3-g" data-slide-to="4"></li><li data-app-prevent-settings="" data-target="#lb-gallery3-g" data-slide-to="5"></li><li data-app-prevent-settings="" data-target="#lb-gallery3-g" data-slide-to="6"></li><li data-app-prevent-settings="" data-target="#lb-gallery3-g" data-slide-to="7"></li><li data-app-prevent-settings="" data-target="#lb-gallery3-g" data-slide-to="8"></li><li data-app-prevent-settings="" data-target="#lb-gallery3-g" data-slide-to="9"></li><li data-app-prevent-settings="" data-target="#lb-gallery3-g" data-slide-to="10"></li><li data-app-prevent-settings="" data-target="#lb-gallery3-g" class=" active" data-slide-to="11"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item">
                            <img alt="" src="<?php echo base_url('assets/images/12762-2000x1334.jpg');?>">
                        </div><div class="carousel-item">
                            <img alt="" src="<?php echo base_url('assets/images/048025-2000x1332.jpg');?>">
                        </div><div class="carousel-item">
                            <img alt="" src="<?php echo base_url('assets/images/signage-2000x1332.jpg');?>">
                        </div><div class="carousel-item">
                            <img alt="" src="<?php echo base_url('assets/images/transform-acrylic-lucite-coffee-table-with-home-decor-arrangement-ideas-with-acrylic-lucite-coffee-table-2000x1331.jpg');?>">
                        </div><div class="carousel-item">
                            <img alt="" src="<?php echo base_url('assets/images/eidos-dda-kiosks-for-hounslow-ccg-2000x1332.jpg');?>">
                        </div><div class="carousel-item">
                            <img alt="" src="<?php echo base_url('assets/images/77523df0bad9ec4b289b3ace0bedcd0c-2000x1333.jpg');?>">
                        </div><div class="carousel-item">
                            <img alt="" src="<?php echo base_url('assets/images/11196-2000x1331.jpg');?>">
                        </div><div class="carousel-item">
                            <img alt="" src="<?php echo base_url('assets/images/stainless-steel-security-guard-booth-manufacture-hot-2000x1332.jpg');?>">
                        </div><div class="carousel-item">
                            <img alt="" src="<?php echo base_url('assets/images/13315682-1728962734009842-8039578979391214225-n-2000x1333.jpg');?>">
                        </div><div class="carousel-item">
                            <img alt="" src="<?php echo base_url('assets/images/5-2000x1334.jpg');?>">
                        </div><div class="carousel-item">
                            <img alt="" src="<?php echo base_url('assets/images/d-k-fabs-image-2000x1331.jpg');?>">
                        </div><div class="carousel-item active">
                            <img alt="" src="<?php echo base_url('assets/images/20130814-2000x1332.jpg');?>">
                        </div>
                    </div>
                    <a class="left carousel-control" role="button" data-slide="prev" href="#lb-gallery3-g">
                        <span class="icon-prev" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" role="button" data-slide="next" href="#lb-gallery3-g">
                        <span class="icon-next" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>

                    <a class="close" href="#" role="button" data-dismiss="modal">
                        <span aria-hidden="true">×</span>
                        <span class="sr-only">Close</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="mbr-section mbr-parallax-background" id="testimonials1-b" style="background-image: url(assets/images/mbr-2000x1511.jpg); padding-top: 120px; padding-bottom: 120px;">

    <div class="mbr-overlay" style="opacity: 0.6; background-color: rgb(34, 34, 34);">
    </div>

        <div class="mbr-section mbr-section__container mbr-section__container--middle">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 text-xs-center">
                        <h3 class="mbr-section-title display-2">WHAT OUR FANTASTIC CUSTOMERS SAY</h3>
                        <small class="mbr-section-subtitle">We have delivered to plenty of customers before! See what they have to say.&nbsp;</small>
                    </div>
                </div>
            </div>
        </div>


    <div class="mbr-testimonials mbr-section mbr-section-nopadding">
        <div class="container">
            <div class="row">

                <div class="col-xs-12 col-lg-4">

                    <div class="mbr-testimonial card mbr-testimonial-lg">
                        <div class="card-block"><p>"Akaar Delivered a my Nameboard, which was made according to my design.&nbsp;</p><p>Simple, and clean!"</p></div>
                        <div class="mbr-author card-footer">
                            <div class="mbr-author-img"><img src="<?php echo base_url('assets/images/bharath-bhansali-160x160-1.jpg');?>" class="img-circle"></div>
                            <div class="mbr-author-name">Bharath</div>
                            <small class="mbr-author-desc">Director - Kreatica Designs</small>
                        </div>
                    </div>
                </div><div class="col-xs-12 col-lg-4">

                    <div class="mbr-testimonial card mbr-testimonial-lg">
                        <div class="card-block">"Akaar is very professional- timely delivery of high quality results. Interaction were all done over phone- they promptly created samples and shared images-instantly adjusted feedbacks and quickly finished the entire work before the actually promised date of delivery.&nbsp;<span style="font-size: 1.125rem; line-height: 2;">"</span></div>
                        <div class="mbr-author card-footer">
                            <div class="mbr-author-img"><img src="<?php echo base_url('assets/images/meghna-karnawat-160x160-1.png');?>" class="img-circle"></div>
                            <div class="mbr-author-name">Meghna</div>
                            <small class="mbr-author-desc">User</small>
                        </div>
                    </div>
                </div><div class="col-xs-12 col-lg-4">

                    <div class="mbr-testimonial card mbr-testimonial-lg">
                        <div class="card-block"><p>"I didn't have to follow up or do anything. After I placed the order, everything just went smoothly and automatically."</p></div>
                        <div class="mbr-author card-footer">
                            <div class="mbr-author-img"><img src="<?php echo base_url('assets/images/screen-shot-2017-06-12-at-5-160x158-1.png');?>" class="img-circle"></div>
                            <div class="mbr-author-name">Vipin</div>
                            <small class="mbr-author-desc">CA at VIpin Associates</small>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

</section>

<section class="mbr-section" id="form1-e" style="background-color: rgb(255, 255, 255); padding-top: 120px; padding-bottom: 120px;">
    
    <div class="mbr-section mbr-section__container mbr-section__container--middle">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-xs-center">
                    <h3 class="mbr-section-title display-2">CONTACT FORM</h3>
                    <small class="mbr-section-subtitle">Bring your ideas to life today!</small>
                </div>
            </div>
        </div>
    </div>
    <div class="mbr-section mbr-section-nopadding">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-lg-10 col-lg-offset-1" data-form-type="formoid">


                    <div data-form-alert="true">
                        <div hidden="" data-form-alert-success="true" class="alert alert-form alert-success text-xs-center">Thanks for filling out form!</div>
                    </div>


                    <form action="https://mobirise.com/" method="post" data-form-title="CONTACT FORM">

                        <input type="hidden" value="4g0vuf16rc6hHDWYOlvdjqBNT7i+ewJbUwnqvjmz2VJTi7KdWmFIKhRWjm/RBgHycCkplWtg+TiNXaXnTA3g/fan5j9pWA51p7Q5mnqOnKmmOcH7J7rXzefaDbJIhr82" data-form-email="true">

                        <div class="row row-sm-offset">

                            <div class="col-xs-12 col-md-4">
                                <div class="form-group">
                                    <label class="form-control-label" for="form1-e-name">Name<span class="form-asterisk">*</span></label>
                                    <input type="text" class="form-control" name="name" required="" data-form-field="Name" id="form1-e-name">
                                </div>
                            </div>

                            <div class="col-xs-12 col-md-4">
                                <div class="form-group">
                                    <label class="form-control-label" for="form1-e-email">Email<span class="form-asterisk">*</span></label>
                                    <input type="email" class="form-control" name="email" required="" data-form-field="Email" id="form1-e-email">
                                </div>
                            </div>

                            <div class="col-xs-12 col-md-4">
                                <div class="form-group">
                                    <label class="form-control-label" for="form1-e-phone">Phone</label>
                                    <input type="tel" class="form-control" name="phone" data-form-field="Phone" id="form1-e-phone">
                                </div>
                            </div>

                        </div>

                        <div class="form-group">
                            <label class="form-control-label" for="form1-e-message">Message</label>
                            <textarea class="form-control" name="message" rows="7" data-form-field="Message" id="form1-e-message"></textarea>
                        </div>

                        <div><button type="submit" class="btn btn-primary">CONTACT US</button></div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="mbr-section mbr-section-md-padding mbr-footer footer1 mbr-background" id="contacts1-c" style="background-image: url(assets/images/footer-pattern-2000x1000-9.png); padding-top: 90px; padding-bottom: 30px;">
    
    <div class="container">
        <div class="row">
            <div class="mbr-footer-content col-xs-12 col-md-3">
                <div><img src="<?php echo base_url('assets/images/logo2-128x32-7.png');?>"></div>
            </div>
            <div class="mbr-footer-content col-xs-12 col-md-3">
                <p></p><p><strong>Contacts</strong><br>Address: <a href="https://www.google.co.in/maps/place/Akaar,+Vishweshwarapura,+Basavanagudi,+Bengaluru,+Karnataka+560004/@12.949425,77.5760983,18z/data=!4m5!3m4!1s0x3bae15ec2872b6af:0x1fb3a9821eeb971b!8m2!3d12.9497451!4d77.5769516" target="_blank" class="text-white">FFD7, 4th floor, Sampoorna Chambers,&nbsp;Vasavi Temple Road, Opposite to Vijaya Bank,&nbsp;V.V Puram, Basavanagudi</a><a href="https://www.google.co.in/maps/place/Akaar,+Vishweshwarapura,+Basavanagudi,+Bengaluru,+Karnataka+560004/@12.949425,77.5760983,18z/data=!4m5!3m4!1s0x3bae15ec2872b6af:0x1fb3a9821eeb971b!8m2!3d12.9497451!4d77.5769516"></a><br>Bangalore - 560004 <br>Email: <a href="mailto:care@akaar.org" class="text-white">care@akaar.org</a><br>Phone: <a href="tel:180030025227" class="text-white">1800 300 25227</a><a href="tel:180030025227"></a><br></p><p></p>
            </div>
            <div class="mbr-footer-content col-xs-12 col-md-3">
                <p><strong>Services</strong><br><a href="http://www.akaar.org/laser" target="_blank" class="text-white">Laser Cutting</a><br><a href="3D Printing" target="_blank" class="text-white">3D Printing</a><br><a href="http://www.akaar.org/cnc_routing" target="_blank" class="text-white">CNC Routing</a><br><a href="http://www.akaar.org/cnc_machining" target="_blank" class="text-white">CNC Machining</a><br><a href="http://www.akaar.org/nameboards" target="_blank" class="text-white">Nameboards</a><br><a href="http://www.akaar.org/metal-fabrication" target="_blank" class="text-white">Metal Fabrication</a><br><a href="http://www.akaar.org/acrylic-fabrication" target="_blank" class="text-white">Acrylic Fabrication</a><br><a href="http://www.akaar.org/custom-manufacturing" target="_blank" class="text-white">Customized Products</a><br><br></p>
            </div>
            <div class="mbr-footer-content col-xs-12 col-md-3">
                <p><strong>Company</strong><br><a href="http://www.akaar.org/about-us" target="_blank" class="text-white">About Us</a><br><a href="http://www.akaar.org/faq" target="_blank" class="text-white">FAQ</a><br><a href="http://www.akaar.org/blog" target="_blank" class="text-white">Blog</a><br><br><br><strong>Partner with us</strong><br><br><a href="http://www.akaar.org/manufacturers" target="_blank" class="text-white">Manufacturers</a><br><a href="http://www.akaar.org/designers" target="_blank" class="text-white">Designers</a><br></p>
            </div>

        </div>
    </div>
</section>

<footer class="mbr-small-footer mbr-section mbr-section-nopadding mbr-background" id="footer1-d" style="background-image: url(assets/images/footer-pattern-2000x1000-10.png); padding-top: 1.75rem; padding-bottom: 1.75rem;">
    <div class="mbr-overlay" style="opacity: 0.3; background-color: rgb(68, 68, 68);"></div>
    <div class="container">
        <p class="text-xs-center"><a href="http://www.akaar.org/terms-and-conditions" target="_blank" class="text-white">&nbsp;TERMS AND CONDITIONS</a> &nbsp; &nbsp; &nbsp; <a href="http://www.akaar.org/privacy-policy" target="_blank" class="text-white">PRIVACY POLICY &nbsp; </a>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Copyright © Akaar. All Rights Reserved.</p>
    </div>
</footer>


  <script src="<?php echo base_url('assets/mobirise/web/assets/jquery/jquery.min.js');?>"></script>
  <script src="<?php echo base_url('assets/mobirise/tether/tether.min.js');?>"></script>
  <script src="<?php echo base_url('assets/mobirise/bootstrap/js/bootstrap.min.js');?>"></script>
  <script src="<?php echo base_url('assets/mobirise/smooth-scroll/smooth-scroll.js');?>"></script>
  <script src="<?php echo base_url('assets/mobirise/dropdown/js/script.min.js');?>"></script>
  <script src="<?php echo base_url('assets/mobirise/touch-swipe/jquery.touch-swipe.min.js');?>"></script>
  <script src="<?php echo base_url('assets/mobirise/viewport-checker/jquery.viewportchecker.js');?>"></script>
  <script src="<?php echo base_url('assets/mobirise/jarallax/jarallax.js');?>"></script>
  <script src="<?php echo base_url('assets/mobirise/typed/typed.min.js');?>"></script>
  <script src="<?php echo base_url('assets/mobirise/masonry/masonry.pkgd.min.js');?>"></script>
  <script src="<?php echo base_url('assets/mobirise/imagesloaded/imagesloaded.pkgd.min.js');?>"></script>
  <script src="<?php echo base_url('assets/mobirise/bootstrap-carousel-swipe/bootstrap-carousel-swipe.js');?>"></script>
  <script src="<?php echo base_url('assets/mobirise/theme/js/script.js');?>"></script>
  <script src="<?php echo base_url('assets/mobirise/mobirise3-blocks-plugin/js/script.js');?>"></script>
  <script src="<?php echo base_url('assets/mobirise/mobirise-gallery/player.min.js');?>"></script>
  <script src="<?php echo base_url('assets/mobirise/mobirise-gallery/script.js');?>"></script>
  <script src="<?php echo base_url('assets/mobirise/formoid/formoid.min.js');?>"></script>
  
  
  <input name="animation" type="hidden">
   <div id="scrollToTop" class="scrollToTop mbr-arrow-up"><a style="text-align: center;"><i class="mbr-arrow-up-icon"></i></a></div>
</body>
</html>